package test01.com.greedy.test01;

public class Application {

	public static void main(String[] args) {

		int a;

		Test1 t1 = new Test1();
		t1.testMethod();
		Test2 t2 = new Test2();
		t2.testMethod();
		Test3 t3 = new Test3();
		t3.testMethod();
		Test4 t4 = new Test4();
		t4.testMethod();
		Test5 t5 = new Test5();
		t5.testMethod();
	}

}
